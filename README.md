# Cookiez

The same idea as [Dino](https://github.com/CuriosityDev/Dino) except instead of the Chrome Dinosaur game it's Cookie Clicker.

Also this time assets are pulled directly from Cookie Clicker's website. No more bundling HTML, CSS, JS, and images.

## Running
Run the following in your terminal (assuming you can run GUI apps):
```
$ git clone https://github.com/CuriosityDev/Cookiez.git
$ cd Cookiez
$ npm run start
```