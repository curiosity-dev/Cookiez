const { app, BrowserWindow } = require('electron');
const Discord = require('discord-rpc');
const path = require('path');
const url = require('url');

if (!String.prototype.padStart) {
    String.prototype.padStart = function padStart(targetLength,padString) {
        targetLength = targetLength>>0; //truncate if number or convert non-number to 0;
        padString = String((typeof padString !== 'undefined' ? padString : ' '));
        if (this.length > targetLength) {
            return String(this);
        }
        else {
            targetLength = targetLength-this.length;
            if (targetLength > padString.length) {
                padString += padString.repeat(targetLength/padString.length); //append to original to ensure we are longer than needed
            }
            return padString.slice(0,targetLength) + String(this);
        }
    };
}

let mainWindow;

function createWindow()
{
	mainWindow = new BrowserWindow({
		width: 1000,
		height: 1000,
		resizable: true,
		titleBarStyle: 'hidden'
	});

	mainWindow.loadURL(url.format({
		pathname: 'orteil.dashnet.org/cookieclicker/index.html',
		protocol: 'http:',
		slashes: true
	}));

	mainWindow.on('closed', () => {
		mainWindow = null;
	});

	mainWindow.on('ready-to-show', () => {
		mainWindow.show();
	});
}

app.on('ready', createWindow);
app.on('window-all-closed', () => {
	app.quit();
});
app.on('activate', () => {
	if (mainWindow === null) {
		createWindow();
	}
});

const currenttime = new Date();
const rpc = new Discord.Client({transport: 'ipc'});
var Discord_Client_ID = process.env.COOKIEZ_DISCORD_CLIENT_ID || '411706729508765716';

async function Update_Presence()
{
	if (!rpc || !mainWindow) return;

	const cookieCount = await mainWindow.webContents.executeJavaScript('~~this.Game.cookies');
	const bakery = await mainWindow.webContents.executeJavaScript('this.Game.bakeryName');
	const cookiesPerSecond = await mainWindow.webContents.executeJavaScript('this.Game.cookiesPs');

	rpc.setActivity({
		details: `Baked ${cookieCount} cookie(s)`,
		state: `Baking ${cookiesPerSecond} cookie(s) per second`,
		startTimestamp: currenttime,
		largeImageKey: 'cookie',
		largeImageText: `In ${bakery}'s bakery`,
		instance: false
	});
}

rpc.on('ready', () => {
	Update_Presence();

	setInterval(() => {
		Update_Presence();
	}, 15e3);
});

rpc.login(Discord_Client_ID).catch(console.error);
process.on('unhandledRejection', promiseErr => {console.error(promiseErr)});
